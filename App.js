import React from 'react'
import { StyleSheet, View, AsyncStorage } from 'react-native'
import { Icon } from 'native-base'
import { Router, Scene, Modal } from 'react-native-router-flux'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'remote-redux-devtools';
import ReduxThunk from 'redux-thunk'

import reducers from './src/reducers'

import HomePage from './src/pages/home'
import DetailPage from './src/pages/detailPage'
import ProfilePage from './src/pages/profilePage'
import TimelinePage from './src/pages/timelinePage'
import SignInPage from './src/pages/signInPage'
import SignUpPage from './src/pages/signUpPage'

import AppFooter from './src/components/appFooter'

import LoginForm from './src/pages/loginForm'


export default class App extends React.Component {
  constructor() {
    super();
    this.state = { hasToken: false };
  }

  // componentWillMount() {
  // AsyncStorage.getItem('token_access').then((token) => {
  //   this.setState({ hasToken: token !== null })
  // })
  // }

  componentDidMount() {
    AsyncStorage.getItem('token_access').then((token) => {
      this.setState({ hasToken: token !== null })
    })
  }

  render() {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    // const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
    const store = createStore(reducers, {}, /* preloadedState, */ applyMiddleware(ReduxThunk))
    
    console.log("Store = ", store.getState());

    const TabIcon = ({ focused, iconName }) => {
      return <Icon name={iconName} style={{ fontSize: 22, color: focused ? '#5E73EB' : '#707070' }} />
    }

    console.log('In APP')
    return (
      // <Provider store={store}>
      //   <LoginForm />
      // </Provider>
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <Router hideNavBar="true">
            <Modal>
              <Scene key="root" hideNavBar={true}>
                <Scene tabs={true} showLabel={false} hideNavBar={true} lazy={true} >
                  <Scene key="home" hideNavBar={true} iconName='home' icon={TabIcon}  >
                    <Scene key="homePage" component={HomePage} hideNavBar={true} />
                    <Scene key="detailPage" component={DetailPage} hideNavBar={true} />
                  </Scene>
                  <Scene key="searchPage" component={HomePage} iconName='search' icon={TabIcon} hideNavBar={true} />
                  <Scene key="timelinePage" component={TimelinePage} iconName='paper' icon={TabIcon} hideNavBar={true} />
                  <Scene key="profilePage" component={ProfilePage} iconName='person' icon={TabIcon} hideNavBar={true} />
                </Scene>
              </Scene>
              <Scene key="signInPage" component={SignInPage} hideNavBar={true} initial={!this.state.hasToken} />
              <Scene key="signUpPage" component={SignUpPage} hideNavBar={true} />
            </Modal>
          </Router>
        </View>
      </Provider>

    )
  }
}

const styles = StyleSheet.create({
  scroll: {
    // paddingHorizontal: 25,
    height: 350
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
