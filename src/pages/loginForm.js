import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, ScrollView, Image, FlatList, ImageBackground, TouchableOpacity, TextInput } from 'react-native'
import { emailChanged } from '../actions'

import SignInCard from '../components/signInCard'
import { Actions } from 'react-native-router-flux';

class LoginForm extends React.Component {
    onEmailChange(text) {
        console.log('===============> ',this.props.email)
        this.props.emailChanged(text);
    }

    render() {
        return (
            <View style={{ flex: 1, top: 200 }}>
                <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                    onChangeText={(text) => this.onEmailChange(text)}
                    value={this.props.email}
                />
                <Text>{this.props.email}</Text>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        email: state.auth.email
    };
};

export default connect(mapStateToProps, { emailChanged })(LoginForm)