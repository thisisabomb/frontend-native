import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';

import DetailCard from '../components/detailCard'
import SliderImgCard from '../components/sliderImgCard'
import AppFooter from '../components/appFooter'

export default class DetailPage extends React.Component {
    constructor(props) {
        super()
        this.state = {
            slug: props.slug
        }
        // console.log(this.state.slug)
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <ScrollView >
                        {/* <SliderImgCard /> */}
                        {/* <View style={{ flex: 1, top: -20, position: 'relative' }}> */}
                            <DetailCard slug={this.state.slug} />
                        {/* </View> */}
                    </ScrollView>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 16
    }
});

module.export = DetailPage
