import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, FlatList, ImageBackground, TouchableOpacity, KeyboardAvoidingView } from 'react-native'

import SignUpCard from '../components/signUpCard'

export default class SignInPage extends React.Component {
    render() {
        return (
            <View style={styles.body}>
                <View style={styles.container}>
                    <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }} enabled>
                        <ScrollView style={{ flex: 1 }}>
                            <View style={styles.headerContainer}>
                                <Text style={styles.title}>Register</Text>
                                {/* <Text style={styles.subTitle}>
                                    Step 1 of 2
                                </Text> */}
                            </View>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
                                <SignUpCard />
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: 'white'
    },
    imgBg: {
        flex: 1,
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },
    container: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    headerContainer: {
        flex: 1,
        marginTop: 95,
        marginLeft: 45
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#6A6A6A',
    },
    subTitle: {
        paddingTop: 10,
        fontSize: 14,
        fontWeight: '200',
        color: '#707070',
    }

})

module.export = SignInPage