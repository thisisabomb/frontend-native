import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity } from 'react-native';

import AppFooter from '../components/appFooter'
import ProfileHeaderCard from '../components/profileHeaderCard'
import TripCard from '../components/tripCard'
import AnimateLine from '../components/animateLine'

export default class ProfilePage extends React.Component {
    constructor(props) {
        super()
        this.state = {
            isPressSelect: false,
        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <ScrollView contentContainerStyle={{ paddingBottom: 25 }}>
                        {/* <View style={{ flex: 1 }}> */}
                        <ProfileHeaderCard />
                        {/* </View> */}
                        <View style={{ marginTop: 170 }}>
                            {/* <Text>test</Text> */}
                            <View style={styles.selectContainer}>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity activeOpacity={0.8} onPress={() => { this.setState({ isPressSelect: false }) }}>
                                        <Text style={styles.selectText}>
                                            All trips
                                        </Text>
                                    </TouchableOpacity>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        {!this.state.isPressSelect ? <AnimateLine value={50}/> : null}
                                    </View>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity activeOpacity={0.8} onPress={() => { this.setState({ isPressSelect: true }) }}>
                                        <Text style={styles.selectText}>
                                            Likes
                                        </Text>
                                    </TouchableOpacity>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        {this.state.isPressSelect ? <AnimateLine value={50}/> : null}
                                    </View>
                                </View>

                            </View>
                            <View style={styles.cardContainer}>
                                <TripCard />
                                <TripCard />
                                <TripCard />
                            </View>
                        </View>
                    </ScrollView>
                </View>

            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 16
    },
    selectContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 20,
        paddingHorizontal: 90
    },
    selectText: {
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold',
        color: '#707070',
        textAlign: 'center'
    },
    cardContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30
    }
});

module.export = ProfilePage
