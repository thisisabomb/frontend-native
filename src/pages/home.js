import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, FlatList, TouchableOpacity, AsyncStorage } from 'react-native';
import { Container, Header } from 'native-base'

import AppFooter from '../components/appFooter'
import HilightCard from '../components/hilightCard'
import PredictTravelCard from '../components/predictTravelCard'
import CategoryCard from '../components/categoryCard'
import { API_SERVICE }from '../constants'
import AnimateLine from '../components/animateLine'

import { Actions } from 'react-native-router-flux';

export default class HomePage extends React.Component {
    state = {
        name: '',
        data: []
    }

    componentDidMount = async () => {
        this.hilightList()
        this.likeAttrList()
        console.log(await AsyncStorage.getItem('LIKE-ATTRS'))
    }

    async saveItem(item, selectedValue) {
        try {
          await AsyncStorage.setItem(item, selectedValue);
        } catch (error) {
          console.error('AsyncStorage error: ' + error.message);
        }
      }

    async userLogout() {
        try {
          await AsyncStorage.removeItem('token_access');
          await AsyncStorage.removeItem('token_refresh');
          await AsyncStorage.removeItem('token_type');
          Actions.signInPage()
          
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
      }
    
    likeAttrList = async () => {
        fetch(API_SERVICE + '/user/attr', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + await AsyncStorage.getItem('token_access')
            }
        }).then(res => res.json())
            .then(data => {
                console.log(data)
                this.saveItem('LIKE-ATTRS', JSON.stringify(data.likeAttrs))
            }).catch((error) => {
                console.error(error);
            });
    }
    
    hilightList = () => {
        console.log(API_SERVICE)
        fetch(API_SERVICE + '/attr', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(res => res.json())
            .then(data => {
                this.setState({ name: data[0].name, data: data })
            }).catch((error) => {
                console.error(error);
            });
    }
    render() {
        return (
            // <Container>
            <View style={styles.container}>
                <View style={{ flex: 1 }}>
                {/* <AnimateLine/> */}
                    <ScrollView
                        contentContainerStyle={{ paddingBottom: 25 }}>
                        <TouchableOpacity activeOpacity={0.8} onPress={ () => { this.userLogout() }}>
                            <Text style={styles.signInButton}>Log out</Text>
                        </TouchableOpacity>
                        <View style={styles.header}>
                            <Text style={styles.textHeader}>
                                TRAVEL FAR ENOUGH {"\n"}
                                TO MEET YOURSELF.
                            </Text>
                            <Image source={require('../components/mon.jpg')} style={styles.imgHeader}></Image>
                        </View>
                        <View style={styles.scroll}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={this.state.data}
                                renderItem={({ item }) => <HilightCard name={item.name} photo={item.photo[0]} detail={item.detail} slug={item.slug} />}
                                keyExtractor={(item, index) => item._id}
                                contentContainerStyle={{paddingLeft: 25, paddingRight: 10}}
                            />
                        </View>
                        <PredictTravelCard />
                        <View style={styles.header}>
                            <Text style={styles.textTitle}>
                                Made for Khathawut
                            </Text>
                            <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#C2C2C2' }}>
                                More
                            </Text>
                        </View>
                        <View style={styles.categoryContainer}>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={{ paddingLeft: 25, paddingRight: 10 }}
                            >
                                <CategoryCard />
                                <CategoryCard />
                                <CategoryCard />
                            </ScrollView>
                        </View>
                    </ScrollView>
                </View>
                {/* <AppFooter /> */}
            </View>
            // </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        paddingTop: 25,
        paddingHorizontal: 25,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    imgHeader: {
        marginTop: 5,
        width: 40,
        height: 40,
        borderRadius: 40 / 2
    },
    textTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#707070'
    },
    textHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#707070'
    },
    scroll: {
        // paddingHorizontal: 25,
        height: 350
    },
    categoryContainer: {
        marginTop: 15
    },
    container: {
        flex: 1,
        paddingTop: 25,
        backgroundColor: 'white'
    },
});

module.export = HomePage
