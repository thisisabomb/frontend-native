import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, FlatList, ImageBackground, TouchableOpacity, KeyboardAvoidingView } from 'react-native'

import SignInCard from '../components/signInCard'
import { Actions } from 'react-native-router-flux';
import { ReactReduxContext } from 'react-redux'

export default class SignInPage extends React.Component {
    // static contextType = ReactReduxContext;
    componentWillMount() {
        // const storeState = this.context.store.getState();
        // console.log(storeState)
        // Actions.reset()
    }

    render() {
        return (
            <View style={styles.body}>
                <View style={styles.container}>
                    <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }} enabled>
                        <ScrollView style={{ flex: 1 }}>
                            <View style={styles.headerContainer}>
                                <Text style={styles.title}>Welcome back</Text>
                                <Text style={styles.subTitle}>
                                    TRAVEL FAR ENOUGH {"\n"}
                                    TO MEET YOURSELF.
                            </Text>
                            </View>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
                                <SignInCard />
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: 'white'
    },
    imgBg: {
        flex: 1,
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },
    container: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    headerContainer: {
        flex: 1,
        marginTop: 95,
        marginLeft: 45
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#6A6A6A',
    },
    subTitle: {
        paddingTop: 10,
        fontSize: 14,
        fontWeight: '200',
        color: '#707070',
    },
    signInButtonContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: 100,
    },
    signInButton: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#8FBC8F',
        letterSpacing: 8,
        width: 250,
        height: 50,
        textAlign: 'center',
        paddingTop: 12,
        borderRadius: 16,
        overflow: "hidden",
        borderColor: 'white',
        backgroundColor: 'white'
    }

})

module.export = SignInPage