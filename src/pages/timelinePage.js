import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native'

import TimelineCard from '../components/timelineCard'

export default class TimelinePage extends React.Component {
    render() {
        return (
            <View style={ styles.body }>
                <ScrollView contentContainerStyle={{ paddingVertical: 20 }}>
                {/* <View style={ styles.headerContainer }> */}
                    <Text style={ styles.headerContainer } > My Feed</Text>
                {/* </View> */}
                <View style={ styles.cardContainer }>
                <TimelineCard/>
                <TimelineCard/>
                <TimelineCard/>
                <TimelineCard/>
                </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerContainer: {
        marginVertical: 30,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#707070',
        textAlign: 'center'
    },
    cardContainer: {
        flex: 5
    },
})

module.export = TimelinePage