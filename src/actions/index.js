import {
    EMAIL_CHANGED,
    PASSWORD_CHANGED,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER
} from './types'
import {
    AsyncStorage
} from 'react-native';
import { API_SERVICE } from '../constants'

const saveItem = async (item, selectedValue) => {
    try {
        await AsyncStorage.setItem(item, selectedValue);
    } catch (error) {
        console.error('AsyncStorage error: ' + error.message);
    }
}

export const emailChanged = (text) => {
    console.log('in action emailChanged')
    return {
        type: EMAIL_CHANGED,
        payload: text
    }
}

export const passwordChanged = (text) => {
    return {
        type: PASSWORD_CHANGED,
        payload: text
    }
}

export const loginUser = ({ email, password }) => {
    console.log('login user',email)
    return (dispatch) => {
        // startLoginUser(dispatch)
        fetch(API_SERVICE + '/signin', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        }).then(res => res.json())
            .then(data => {
                if (data.success) {
                    console.log(data)
                    this.saveItem('token_access', data.token.access)
                    this.saveItem('token_refresh', data.token.refresh)
                    this.saveItem('token_type', data.token.type)
                    dispatch({ type: 'LOGIN_USER_SUCCESS', payload: user });
                    Actions.home()
                }
            }).catch((error) => {
                console.error(error);
            });

        // firebase.auth().signInWithEmailAndPassword(email, password)
        //     .then((user) => loginUserSuccess(dispatch, user))
        //     .catch(() => {
        //         firebase.auth().createUserWithEmailAndPassword(email, password)
        //             .then((user) => loginUserSuccess(dispatch, user))
        //             .catch(() => loginUserFail(dispatch));
        //     })
    }
}

const startLoginUser = (dispatch) => {
    dispatch({ type: LOGIN_USER })
};

const loginUserFail = (dispatch) => {
    dispatch({ type: LOGIN_USER_FAIL })
}

const loginUserSuccess = (dispatch, user) => {
    dispatch({ type: LOGIN_USER_SUCCESS, payload: user })
}