import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements';


export default class TimelineCard extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.cardContainer}>
                    <View style={styles.navContainer}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Image source={require('../assets/eden.jpg')} style={styles.imgUser}></Image>
                            <Text style={styles.nameUser}>คฑาวุฒิ ชัยประพันธ์</Text>
                        </View>
                        <Text style={styles.time}>
                            26m
                        </Text>
                    </View>
                    <View style={{ flex: 5, marginTop: 5 }}>
                        <Image source={require('../assets/wallpaper_1.jpg')} style={{ marginTop: 10, height: 150, width: '100%' }}></Image>
                        <View style={styles.fav}>
                            <Icon name='favorite-border' />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginBottom: 20
        // justifyContent: 'center',
        // height: 145,
        // width: '80%'
    },
    cardContainer: {
        width: '90%',
        height: 250,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowOffset: { width: 5, height: 5, },
        shadowRadius: 20,
        shadowColor: 'black',
        shadowOpacity: 0.1,
        // marginHorizontal: 100
    },
    navContainer: {
        flex: 1,
        paddingTop: 5,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    imgUser: {
        marginTop: 5,
        width: 40,
        height: 40,
        borderRadius: 40 / 2
    },
    nameUser: {
        paddingTop: 12,
        paddingLeft: 10,
        fontWeight: 'bold',
        fontSize: 18,
        color: 'black'
    },
    time: {
        paddingTop: 17,
        color: '#707070'
    },
    fav: {
        paddingTop: 7,
        paddingRight: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
})

module.export = TimelineCard