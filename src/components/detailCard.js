import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native'

import { Icon } from 'react-native-elements';

import SliderImgCard from '../components/sliderImgCard'
import { API_SERVICE } from '../constants'
import NavBar from '../components/navbarDetail'
import InformationCard from '../components/informationCard'
import AnimateLine from '../components/animateLine'

export default class DetailCard extends Component {

    constructor(props) {
        super()
        this.state = {
            name: null,
            province: null,
            detail: null,
            travelType: null,
            photo: null,
            detailName: null,
            dayOpen: null,
            timeOpen: null,
            phone: null,
            isPressSelect: false,
        }
    }

    componentWillMount() {
        const slug = this.props.slug
        console.log(slug)
        fetch(API_SERVICE + '/attr/' + slug, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(res => res.json())
            .then(data => {
                console.log('data: ', data)
                this.setState({
                    name: data.name,
                    detail: data.detail,
                    province: data.province,
                    travelType: data.travelType,
                    photo: data.photo,
                    detailName: data.detailName,
                    dayOpen: data.dayOpen,
                    timeOpen: data.timeOpen,
                    phone: data.phone
                })
                // console.log('data state: ', this.state.data)
            }).catch((error) => {
                console.error(error);
            });
    }
    render() {
        return (
            <View style={styles.container}>
                <SliderImgCard photo={this.state.photo} />
                <NavBar />
                <View style={{
                    flex: 1, top: -20, position: 'relative', backgroundColor: 'white',
                    borderRadius: 16
                }}>
                    <View style={styles.header}>
                        <View style={styles.headerLine1}>
                            <Text style={styles.titleName}>
                                {this.state.name}
                            </Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Text style={{ fontSize: 14, color: '#707070' }}>
                                    จังหวัด :
                            </Text>
                                <Text style={styles.categoryName}>
                                    {this.state.province}
                                </Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: "flex-end" }}>
                                <Text style={{ fontSize: 14, color: '#707070', }}>
                                    หมวดหมู่ :
                                </Text>
                                <Text style={styles.categoryName}>
                                    {this.state.travelType}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.header}>
                        <Text numberOfLines={10} style={styles.detailText}>
                            {this.state.detail}
                        </Text>
                    </View>
                    <View style={styles.selectContainer}>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity activeOpacity={0.8} onPress={() => { this.setState({ isPressSelect: false }) }}>
                                <Text style={styles.selectText}>
                                    ข้อมูลติดต่อ
                            </Text>
                            </TouchableOpacity>
                            <View style={{ flex: 1, alignItems: 'center'}}>
                                {!this.state.isPressSelect ? <AnimateLine value={100}/> : null}
                            </View>
                        </View>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity activeOpacity={0.8} onPress={() => { this.setState({ isPressSelect: true }) }}>
                                <Text style={styles.selectText}>
                                    แผนที่
                                </Text>
                            </TouchableOpacity>
                            <View style={{ flex: 1, alignItems: 'center'}}>
                                {this.state.isPressSelect ? <AnimateLine value={100}/> : null}
                            </View>
                        </View>
                    </View>
                    <InformationCard detailName={this.state.detailName} dayOpen={this.state.dayOpen} timeOpen={this.state.timeOpen} phone={this.state.phone} />
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        marginTop: 25,
        marginHorizontal: 25
    },
    headerContainer: {
        flex: 1,
        // backgroundColor: 'red', 
        position: 'absolute',
        left: 0,
        top: 50,
        height: 50,
        flexDirection: 'row',
        alignContent: 'space-between'
    },
    navbar: {
        position: 'relative',
        marginHorizontal: 20,
        flexDirection: 'row',
        // justifyContent: 'space-between'
    },
    titleName: {
        fontSize: 32,
        fontWeight: 'bold',
        color: '#707070'
    },
    provinceName: {
        paddingTop: 15,
        fontSize: 14,
        fontWeight: '200',
        color: '#C49B2B'
    },
    categoryName: {
        paddingLeft: 5,
        fontSize: 14,
        fontWeight: '200',
        color: '#C49B2B'
    },
    headerLine1: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    detailText: {
        fontSize: 16,
        color: '#919191',
        lineHeight: 30,
        // marginBottom: 100
    },
    selectContainer: {
        flex: 1,
        marginVertical: 30,
        marginHorizontal: '15%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    selectText: {
        alignSelf: 'center',
        marginBottom: 5,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#808080'
    }
});

module.export = DetailCard