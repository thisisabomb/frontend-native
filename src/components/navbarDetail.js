import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, TouchableOpacity } from 'react-native'

import { Icon } from 'react-native-elements';

import SliderImgCard from '../components/sliderImgCard'
import { API_SERVICE } from '../constants'
import { Actions } from 'react-native-router-flux'

export default class NavBar extends Component {
    constructor() {
        super()
        this.state = {
            isPressFav: false
        }
    }
    
    render() {
        const nameFav =  this.state.isPressFav ? 'favorite' : 'favorite-border'
        return (
            <View style={styles.headerContainer}>
                {/* <View style={styles.navbar}> */}
                <View style={{ flex: 1, alignItems: 'flex-start' }}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => { Actions.pop() }}>
                        <Icon size={30} color='white' name='arrow-back' />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => { this.setState({ isPressFav: !this.state.isPressFav }) }}>
                        <Icon size={30} color='white' name={nameFav} />
                    </TouchableOpacity>
                </View>
                {/* </View> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        flex: 1,
        position: 'absolute',
        flexDirection: 'row',
        alignContent: 'space-between',
        paddingHorizontal: 20,
        top: 50
    }
})