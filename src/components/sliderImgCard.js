import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, ScrollView, FlatList } from 'react-native';

export default class SliderImgCard extends Component {
    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={this.props.photo}
                    renderItem={({ item }) => <Image style={{ width: '100%', height: 340 }} source={item != null ? { uri: item } : require('../components/mon.jpg')}/>}
                    keyExtractor={(item, index) => 'key' + index }
                    contentContainerStyle={{ height: 340, width: '100%' }}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

module.export = SliderImgCard