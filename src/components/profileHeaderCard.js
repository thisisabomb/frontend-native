import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, TouchableOpacity, AsyncStorage } from 'react-native';

import { API_SERVICE } from '../constants'


export default class ProfileHeaderCard extends Component {
    constructor() {
        super()
        this.state = {
            follower: null,
            following: null,
            trip: null,
            firstName: null,
            lastName: null,
            description: null
        }
    }

    componentWillMount() {
        this.getProfile()
        // AsyncStorage.getItem('token_access').then((token) => {
        //     this.setState({ hasToken: token !== null })
        //   })
        // this.getProfile()
    }

    getProfile = async () => {
        // console.log(await AsyncStorage.getItem('token_access'))
        fetch(API_SERVICE + '/profile', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + await AsyncStorage.getItem('token_access')
            }
        }).then(res => res.json())
            .then(data => {
                // console.log(data)
                this.setState({
                    follower: data.amount.follower,
                    following: data.amount.following,
                    trip: data.amount.trip,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    description: data.description
                })
            }).catch((error) => {
                console.error(error);
            })
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../assets/wallpaper_1.jpg')} style={styles.imageCard}>
                    <View style={styles.detailContainer}>
                        <View style={styles.detail}>
                            <Text style={styles.name}>
                                {this.state.firstName + ' ' + this.state.lastName}
                            </Text>
                            <Text style={styles.des}>
                                {this.state.description}
                            </Text>
                            <View style={styles.numberContainer}>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={styles.number}>
                                        {this.state.follower}
                                    </Text>
                                    <Text style={styles.text}>
                                        FOLLOWER
                                    </Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={styles.number}>
                                        {this.state.trip}
                                    </Text>
                                    <Text style={styles.text}>
                                        TRIPS
                                    </Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={styles.number}>
                                        {this.state.following}
                                    </Text>
                                    <Text style={styles.text}>
                                        FOLLOWING
                                    </Text>
                                </View>
                            </View>
                            <TouchableOpacity>
                                <View style={styles.followButtonContainer}>
                                    <Text style={styles.followButton}>
                                        Follow
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.profileImgContainer}>
                            <Image source={require('../assets/eden.jpg')} style={styles.profileImg} />
                        </View>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageCard: {
        // width: 375,
        height: 215
    },
    detailContainer: {
        flex: 1,
        alignItems: 'center',
        top: 125,
        shadowOffset: { width: 5, height: 5, },
        shadowRadius: 20,
        shadowColor: 'black',
        shadowOpacity: 0.1,
        // marginHorizontal: 100
        // borderRadius: 16
    },
    detail: {
        position: 'absolute',
        width: '80%',
        height: 245,
        paddingTop: 75,
        borderWidth: 1,
        borderRadius: 16,
        overflow: "hidden",
        borderColor: 'white',
        backgroundColor: 'white'
    },
    profileImg: {
        position: 'relative',
        top: -65,
        width: 130,
        height: 130,
        borderRadius: 130 / 2,
    },
    profileImgContainer: {
        shadowOffset: { width: 5, height: 5, },
        shadowRadius: 20,
        shadowColor: 'black',
        shadowOpacity: 0.1,
    },
    numberContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 15,
        paddingHorizontal: 25,
    },
    name: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: '#707070'
    },
    des: {
        marginTop: 5,
        textAlign: 'center',
        fontSize: 12,
        fontWeight: 'normal',
        color: '#ACACAC'
    },
    number: {
        fontSize: 26,
        fontWeight: '600',
        color: '#707070'
    },
    text: {
        fontSize: 10,
        fontWeight: '100',
        color: '#707070'
    },
    followButtonContainer: {
        alignItems: 'center',
        paddingTop: 15,
        shadowOffset: { width: 1, height: 1 },
        shadowRadius: 5,
        shadowColor: 'black',
        shadowOpacity: 0.1,
    },
    followButton: {
        paddingTop: 8,
        textAlign: 'center',
        width: 128,
        height: 32,
        color: 'white',
        fontWeight: 'bold',
        backgroundColor: '#6DC9F2',
        borderRadius: 8,
        overflow: "hidden",
        borderColor: '#6DC9F2',
    }
});

module.export = ProfileHeaderCard