import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, ScrollView } from 'react-native'

import { Icon } from 'react-native-elements';

import SliderImgCard from '../components/sliderImgCard'
import { API_SERVICE } from '../constants'

export default class InformationCard extends Component {
    render() {
        return(
            <View style={styles.container}>
                <Text style={styles.infoText}>
                    {this.props.detailName}{'\n'}
                    {this.props.dayOpen}{'\n'}
                    {this.props.timeOpen}{'\n'}
                    {this.props.phone}{'\n'}
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    infoText: {
        fontSize: 16,
        color: '#919191',
        lineHeight: 30,
        textAlign: 'center'
    }
})
