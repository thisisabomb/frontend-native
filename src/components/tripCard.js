import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux';

export default class TripCard extends Component {
    render() {
        return (
            // <TouchableOpacity activeOpacity={1.0} onPress={() => Actions.detailPage()}>
                <View style={styles.container}>
                    <ImageBackground source={require('./mon.jpg')} style={styles.hilightCardImg} imageStyle={{ borderRadius: 16 }}>
                        <View style={styles.rightContainer}>
                            <Icon name= 'map-marker' type= 'font-awesome' size={18} color='white'/>
                            <Text style={styles.detailRight}>
                                เชียงใหม่
                            </Text>
                        </View>
                        <View style={styles.dateContainer}>
                            <Text style={styles.dateRight}>
                                01-01-2019
                            </Text>
                        </View>
                        <View style={styles.titleContainer}>
                            <Text style={styles.dayLeft}>
                                3 วัน 2 คืน
                            </Text>
                            <Text style={styles.titleLeft}>
                                ทุ่งหญ้าสีทองที่ดอยม่องจอง
                            </Text>
                        </View>
                    </ImageBackground>
                </View>
            // </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 20
        // paddingTop: 15,
        // marginRight: 20
    },
    hilightCardImg: {
        width: 325,
        height: 145,
        shadowOffset: { width: 5, height: 5, },
        shadowRadius: 20,
        shadowColor: 'black',
        shadowOpacity: 0.1,
        // marginHorizontal: 100
    },
    titleContainer: {
        paddingLeft: 15,
        top: 20
    },
    rightContainer: {
        flexDirection: 'row', 
        justifyContent: 'flex-end',
        paddingTop: 15,
        paddingRight: 15
    },
    dateContainer: {
        flexDirection: 'row', 
        justifyContent: 'flex-end',
        // paddingTop: 2,
        paddingRight: 15
    },
    detailRight: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        paddingLeft: 5,
        bottom: 5
    },
    dateRight: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'white',
        bottom: 5
    },
    dayLeft: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'white',
    },
    titleLeft: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
    }
});

module.export = TripCard