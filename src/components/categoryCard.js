import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, TouchableOpacity } from 'react-native'

export default class CategoryCard extends Component {
    render() {
        return(
            <View style={styles.container}>
                <Image source={require('./mon.jpg')} style={ styles.img }/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    img: {
        width: 105,
        height: 105,
        borderRadius: 16
    },
    container: {
        marginRight: 13
    }
})

module.export = CategoryCard