import React, { Component, } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Animated,
    TextInput,
    KeyboardAvoidingView,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';
import { emailChanged, passwordChanged, loginUser } from '../actions'
import { connect } from 'react-redux';
import { API_SERVICE } from '../constants'

import { LinearGradient } from 'expo'


import InputCard from '../components/inputCard'
import { Actions } from 'react-native-router-flux';


class SignInCard extends Component {
    state = {
        email: '',
        password: '',
    }

    async saveItem(item, selectedValue) {
        try {
            await AsyncStorage.setItem(item, selectedValue);
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    handleEmailInput = (text) => {
        // console.log('Email=>', text)
        // this.props.dispatch({type: 'email_changed',
            // payload: text})
        this.props.emailChanged(text);

    }

    handlePasswordInput = (text) => {
        // console.log('Password=>', this.props.password)
        this.props.passwordChanged(text);
    }

    // handleInput = (text, label) => {
    //     console.log(`${label}: `, text)
    //     this.props.emailChanged(text);
    //     if (label == 'E-mail') {
    //         this.setState({ email: text })
    //     } else {
    //         this.setState({ password: text })
    //     }
    // }

    submitInput = () => {
        const { email , password } = this.props
        // console.log('in: ', this.props)
        this.props.loginUser({ email, password })
        // fetch(API_SERVICE + '/signin', {
        //     method: 'POST',
        //     headers: {
        //         Accept: 'application/json',
        //         'Content-Type': 'application/json',
        //     },
        //     body: JSON.stringify({
        //         email: this.state.email,
        //         password: this.state.password,
        //     }),
        // }).then(res => res.json())
        //     .then(data => {
        //         if (data.success) {
        //             console.log(data)
        //             this.saveItem('token_access', data.token.access)
        //             this.saveItem('token_refresh', data.token.refresh)
        //             this.saveItem('token_type', data.token.type)
        //             // this.forceUpdate()
        //             // Actions.pop({key: 'homePage'})
        //             // Actions.refresh({ key: Actions.homePage()})
        //             Actions.home()
        //         }
        //     }).catch((error) => {
        //         console.error(error);
        //     });
    }

    render() {
        return (
            <View style={{ flex: 1, width: '80%' }}>
                <InputCard label='E-mail' handleInput={this.handleEmailInput} password={false} />
                <InputCard label='Password' handleInput={this.handlePasswordInput} password={true} />
                <View style={styles.signInContainer}>
                    <TouchableOpacity activeOpacity={0.8} onPress={this.submitInput}>
                        <LinearGradient
                            colors={['#BFB4FF', '#4B3DF8']}
                            style={{ height: 50, borderRadius: 10 }}
                            start={{ x: 1, y: 0 }}
                            end={{ x: 0, y: 1 }}
                        >
                            <Text style={styles.signInButton}>SIGN IN</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={styles.signUpText}>
                        Don't have an account?
                    </Text>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => { Actions.signUpPage() }}>
                        <Text style={styles.signUpTouch}>
                            {' '}Sign up
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ flex: 1, borderBottomColor: '#707070', borderBottomWidth: 1 }} />
                    <Text style={{ fontSize: 12, paddingHorizontal: 10, top: 5, color: '#707070' }}>OR</Text>
                    <View style={{ flex: 1, borderBottomColor: '#707070', borderBottomWidth: 1 }} />
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                    <Text style={styles.facebook}>Facebook</Text>
                    <Text style={styles.google}>Google</Text>
                </View>
            </View>
        )
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333'
    },
    list: {
        marginTop: 30,
        marginLeft: 5
    },
    myInputStyle: {
        borderBottomWidth: 1,
        width: 300,
        height: 70
    },
    textInput: {
        height: 60,
        fontSize: 26,
        color: '#eee'
    },
    signInContainer: {
        marginTop: 20
    },
    signInButton: {
        paddingTop: 15,
        fontSize: 16,
        fontWeight: '400',
        textAlign: 'center',
        color: 'white'
    },
    signUpText: {
        paddingVertical: 10,
        fontSize: 12,
        fontWeight: '100',
        color: '#707070',
        textAlign: 'center'
    },
    signUpTouch: {
        paddingVertical: 10,
        fontSize: 12,
        fontWeight: '700',
        color: '#707070',
    },
    facebook: {
        paddingTop: 10,
        color: 'white',
        width: 140,
        height: 40,
        backgroundColor: '#48629C',
        borderRadius: 10,
        overflow: "hidden",
        borderColor: '#48629C',
        textAlign: 'center'
    },
    google: {
        paddingTop: 10,
        color: 'white',
        width: 140,
        height: 40,
        backgroundColor: '#DB4B38',
        borderRadius: 10,
        overflow: "hidden",
        borderColor: '#DB4B38',
        textAlign: 'center'
    }
});

function mapStateToProps(state) {
    console.log('+++++++++in map: ', state)
    return {
      email: state.auth.email,
      password: state.auth.password
    }
  }

export default connect( mapStateToProps, {
    emailChanged,
    passwordChanged,
    loginUser
})(SignInCard);

// module.export = SignInCard