import React, { Component, } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, PickerIOS } from 'react-native'
import { LinearGradient } from 'expo'
import { Actions } from 'react-native-router-flux';
import ReactNativePickerModule from 'react-native-picker-module'
import DateTimePicker from 'react-native-modal-datetime-picker';

import InputCard from '../components/inputCard'
import StepIndicator from 'react-native-step-indicator';
import { API_SERVICE }from '../constants'

export default class SignUpCard extends Component {
    state = {
        email: '',
        password: '',
        confPassword: '',
        firstName: '',
        lastName: '',
        gender: '',
        nextStep: false,
        currentPosition: 0,
        dataGender: [
            'Male',
            'Female',
            'Others'
        ],
        selectedValue: null,
        birthday: new Date(),
        isDateTimePickerVisible: false,
    }

    handleInput = (text, label) => {
        console.log(`${label}: `, text)
        // emailChanged(text)
        if (label == 'E-mail') {
            this.setState({ email: text })
        } else if (label == 'Password') {
            this.setState({ password: text })
        } else if (label == 'Confirm password') {
            this.setState({ confPassword: text })
        } else if (label == 'First Name') {
            this.setState({ firstName: text })
        } else if (label == 'Last Name') {
            this.setState({ lastName: text })
        }

    }

    submitNextStep = () => {
        if (this.state.email != '' && this.state.password != '' && this.state.confPassword != '') {
            fetch(API_SERVICE + '/check-email', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
              }),
        }).then(res => res.json())
            .then(data => {
                // console.log(data)
                if (data.success) {
                    this.setState({ nextStep: true, currentPosition: 1 })
                }
                else {
                    console.log(data)
                }
            }).catch((error) => {
                console.error(error);
            });
            // this.setState({ nextStep: true, currentPosition: 1 })
            // console.log(this.state)
        }
    }

    submitInput = () => {
        console.log(this.state)
        console.log('in: ',this.state.email)
        fetch(API_SERVICE + '/signup', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                gender: this.state.gender,
                birthday: this.state.birthday
              }),
        }).then(res => res.json())
            .then(data => {
                if (data.success) {
                    console.log(data)
                    // this.saveItem('token_access', data.token.access)
                    // this.saveItem('token_refresh', data.token.refresh)
                    // this.saveItem('token_type', data.token.type)
                    // this.forceUpdate()
                    // Actions.pop({key: 'homePage'})
                    // Actions.refresh({ key: Actions.homePage()})
                    // Actions.homePage()
                }
            }).catch((error) => {
                console.error(error);
            });
    }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this.setState({ birthday: date })
        this._hideDateTimePicker();
    };

    render() {
        const customStyles = {
            stepIndicatorSize: 25,
            currentStepIndicatorSize: 30,
            separatorStrokeWidth: 2,
            currentStepStrokeWidth: 3,
            stepStrokeCurrentColor: '#6E61F9',
            stepStrokeWidth: 3,
            stepStrokeFinishedColor: '#6E61F9',
            stepStrokeUnFinishedColor: '#aaaaaa',
            separatorFinishedColor: '#6E61F9',
            separatorUnFinishedColor: '#aaaaaa',
            stepIndicatorFinishedColor: '#6E61F9',
            stepIndicatorUnFinishedColor: '#ffffff',
            stepIndicatorCurrentColor: '#ffffff',
            stepIndicatorLabelFontSize: 13,
            currentStepIndicatorLabelFontSize: 13,
            stepIndicatorLabelCurrentColor: '#6E61F9',
            stepIndicatorLabelFinishedColor: '#ffffff',
            stepIndicatorLabelUnFinishedColor: '#aaaaaa',
            labelColor: '#999999',
            labelSize: 13,
            currentStepLabelColor: '#fe7013'
        }

        return (
            <View style={styles.body}>
                <View style={{ top: -30 }}>
                    <StepIndicator
                        stepCount={2}
                        customStyles={customStyles}
                        currentPosition={this.state.currentPosition}
                    />
                </View>
                {/* {!this.state.nextStep ? */}
                <View>
                    {!this.state.nextStep ? <InputCard label='E-mail' handleInput={this.handleInput} /> : null}
                    {!this.state.nextStep ? <InputCard label='Password' handleInput={this.handleInput} /> : null}
                    {!this.state.nextStep ? <InputCard label='Confirm password' handleInput={this.handleInput} /> : null}
                    {!this.state.nextStep ? null : <InputCard label='First Name' handleInput={this.handleInput} />}
                    {!this.state.nextStep ? null : <InputCard label='Last Name' handleInput={this.handleInput} />}
                    {!this.state.nextStep ? null :
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity onPress={() => { this.pickerRef.show() }}>
                                <View style={styles.genderPicker}>
                                    <Text style={styles.genderText}>{this.state.gender != '' ? this.state.gender : 'Gender'}</Text>
                                    <View style={{ flex: 1, borderBottomColor: '#707070', borderBottomWidth: 1 }} />
                                </View>
                            </TouchableOpacity>
                            <ReactNativePickerModule
                                pickerRef={e => this.pickerRef = e}
                                value={this.state.selectedValue}
                                title={"Select gender"}
                                items={this.state.dataGender}
                                onValueChange={(index) => {
                                    this.setState({
                                        selectedValue: index,
                                        gender: this.state.dataGender[index]
                                    })
                                }} />
                        </View>
                    }
                    {!this.state.nextStep ? null :
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity onPress={this._showDateTimePicker}>
                                <View style={styles.birthPicker}>
                                    <Text style={styles.genderText}>Birthday</Text>
                                    <View style={{ flex: 1, borderBottomColor: '#707070', borderBottomWidth: 1 }} />
                                </View>
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this._handleDatePicked}
                                onCancel={this._hideDateTimePicker}
                            />
                        </View>
                    }
                    <View style={styles.signUpContainer}>
                        <TouchableOpacity activeOpacity={0.8} onPress={() => { !this.state.nextStep ? this.submitNextStep() : this.submitInput() }}>
                            <LinearGradient
                                colors={(this.state.email != ''
                                    && this.state.password != ''
                                    && this.state.confPassword != '') ? ['#BFB4FF', '#4B3DF8'] : ['#DCDCDC', '#A9A9A9']}
                                style={{ width: 290, height: 50, borderRadius: 10 }}
                                start={{ x: 1, y: 0 }}
                                end={{ x: 0, y: 1 }}
                            >
                                <Text style={styles.signUpButton}>{this.state.nextStep ? 'SIGN UP' : 'NEXT'}</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        // backgroundColor: 'red'
    },
    signUpContainer: {
        // flex: 1,
        // justifyContent: 'flex-end'
        marginTop: '15%'
    },
    signUpButton: {
        paddingTop: 15,
        fontSize: 16,
        fontWeight: '400',
        textAlign: 'center',
        color: 'white'
    },
    genderPicker: {
        marginTop: 30,
    },
    genderText: {
        fontSize: 16,
        color: '#999',
        marginBottom: 16,
    },
    birthPicker: {
        marginTop: 20
    }
}) 