import React, { Component, } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Animated,
    TextInput,
    KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../actions'

export default class InputCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isValid: null,
            text: ''
        }
        this.animatedValue = new Animated.Value(0);
    }
    componentWillMount() {
        this.inputValue = '';
        this.setState({ isValid: null, text: ''})
    }
    onChangeText(text) {
        this.inputValue = text;
        this.setState({ text: text })
        // console.log('###############',this.props.email)
        // this.props.emailChanged(text)
        this.props.handleInput(text)
        this.setState({ isValid: this.inputValue.length > 0 })
    }
    selectField() {
        Animated.timing(this.animatedValue, {
            toValue: 100,
            duration: 200
        }).start();
    }
    deselectField() {
        if (this.inputValue.length > 0) {
            return;
        }
        Animated.timing(this.animatedValue, {
            toValue: 0,
            duration: 200
        }).start();
    }
    render() {
        let that = this;
        let interpolatedLabelPosition = that.animatedValue.interpolate({
            inputRange: [0, 180],
            outputRange: [35, 0]
        });
        let interpolatedLabelSize = that.animatedValue.interpolate({
            inputRange: [0, 100],
            outputRange: [16, 14]
        });
        let borderColor = (this.state.isValid === null) ? '#999' : ((this.state.isValid === true) ? '#6E61F9' : '#999');
        let borderBottomWidth = (this.state.isValid === null) ? 1 : ((this.state.isValid === true) ? 4 : 1);
        let color = '#999';
        return (
            <View style={[styles.myInputStyle, { borderColor: borderColor, borderBottomWidth: borderBottomWidth }]}>
                <Animated.Text
                    style={{ fontSize: interpolatedLabelSize, top: interpolatedLabelPosition, color: color }}>
                    {this.props.label}
                </Animated.Text>
                <TextInput
                    secureTextEntry={this.props.password}
                    style={styles.textInput}
                    onFocus={() => { console.log('in Focus'), this.selectField() }}
                    onBlur={() => { console.log('in Blur'), this.deselectField() }}
                    // onChangeText={(text) => { this.props.handleInput(text) }}
                    onChangeText={(text) => { this.onChangeText(text) }}
                    blurOnSubmit={true}
                    value={this.state.text}
                />
            </View>
        )
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333'
    },
    list: {
        marginTop: 30,
        marginLeft: 5
    },
    myInputStyle: {
        borderBottomWidth: 1,
        // width: 290,
        height: 70
    },
    textInput: {
        height: 60,
        fontSize: 20,
        color: '#6E61F9'
    }
});

// const mapStateToProps = state => {
//     console.log('in map: ', state)
//     return {
//       email: state.auth.email,
//       password: state.auth.password
//     }
//   }

// export default connect()(InputCard);

// export default InputCard

// module.export = InputCard