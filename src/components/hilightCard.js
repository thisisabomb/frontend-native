import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body } from 'native-base';
import { Actions } from 'react-native-router-flux';

// const HilightCard = (props) => 
export default class HilightCard extends Component {
    constructor(props) {
        super()
        this.state = {
            slug: props.slug
        }
    }
    render() {
        return (
            <TouchableOpacity activeOpacity={1.0} onPress={ () => Actions.detailPage({ slug: this.state.slug }) }>
            <View style={styles.container}>
            {/* source={ require('./mon.jpg') } */}
                <ImageBackground source={ {uri: this.props.photo } } style={styles.hilightCardImg} imageStyle={{ borderRadius: 16, resizeMode: 'cover' }}>
                    <View style={styles.detailContainer}>
                        <View style={styles.detail}>
                            <Text style={styles.name} >
                                {this.props.name}
                            </Text>
                            <Text numberOfLines={2} ellipsizeMode ={'tail'} style={styles.subDetail} >
                                {this.props.detail}
                                {/* ฟหก่ดาฟหกสด่ฟาหกสดฟ่าหกสด่ฟาสหก่ดฟาสหก่ดฟาสก่ดรไนำพรน */}
                            </Text>
                        </View>
                    </View>
                </ImageBackground>
            </View>
            </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        // marginLeft: 20,
        marginRight: 20,
        // alignItems: 'center'
    },
    hilightCardImg: {
        width: 315,
        height: 230,
        // backgroundColor: 'red'
    },
    detailContainer: {
        flex: 1,
        alignItems: 'center',
        marginTop: 170,
        shadowOffset: { width: 5, height: 15, },
        shadowRadius: 30,
        shadowColor: 'black',
        shadowOpacity: 0.4,
    },
    detail: {
        width: 235,
        height: 95,
        padding: 10,
        borderWidth: 1,
        borderRadius: 16,
        overflow: "hidden",
        borderColor: 'white',
        backgroundColor: 'white',
        textAlign: 'center'
    },
    name: {
        // textAlign: 'center',
        paddingVertical: 5,
        paddingHorizontal: 20,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#707070',
        // marginBottom: 5,
    },
    subDetail: {
        paddingHorizontal: 20,
        fontSize: 12,
        fontWeight: '200',
        color: '#707070'
    }
});

module.export = HilightCard