import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, ScrollView, Animated } from 'react-native'

export default class AnimateLine extends Component {
    constructor(props) {
        super(props);
        this.width = new Animated.Value(0);
    }

    animateBar = () => {
        const { value, index } = this.props;
        this.width.setValue(0); // initialize the animated value
        Animated.timing(this.width, {
            toValue: value,
            delay: 1 * 150 // how long to wait before actually starting the animation
        }).start();
    }

    componentDidMount(){
        this.animateBar()
    }
    render() {
        let barWidth = {
            width: this.width
        }
        return (
            <Animated.View style={[styles.bar, barWidth]} />
        )
    }
}

const styles = StyleSheet.create({
    bar: {
        height: 5,
        borderWidth: 1,
        borderColor: "#C49B2B",
        backgroundColor: "#C49B2B",
        borderRadius: 10,
    }
})