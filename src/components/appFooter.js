import React, { Component } from 'react'
import { AsyncStorage, View } from 'react-native'
import { Container, Header, Content, Footer, FooterTab, Button, Text, Icon } from 'native-base'
import { Actions } from 'react-native-router-flux';

export default class AppFooter extends Component {
    constructor() {
        super();
        this.state = { hasToken: false };
      }

    componentWillMount() {
        AsyncStorage.getItem('token_access').then((token) => {
          this.setState({ hasToken: token !== null })
        })
      }

    render() {
        return (
            // <Container>
            //      <Content />
            // {!this.state.hasToken ? null:
            // }
            <View>
                {!this.state.hasToken ? null:
                <Footer>
                <FooterTab>
                    <Button onPress= { () => Actions.homePage()}>
                        <Icon name='home' style={{color: '#5E73EB'}} />
                    </Button>
                    <Button>
                        <Icon name='search' />
                    </Button>
                    <Button onPress= { () => Actions.timelinePage()}>
                        <Icon name="paper" />
                    </Button>
                    <Button onPress= { () => Actions.profilePage()}>
                        <Icon name='person' />
                    </Button>
                </FooterTab>
            </Footer>
                }
                
            </View>
                
            // </Container>
        )
    }
}

module.export = AppFooter