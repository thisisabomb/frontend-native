import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, ImageBackground, TouchableOpacity } from 'react-native'

export default class PredictTravelCard extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                    <ImageBackground source={require('./mon.jpg')} style={styles.img} imageStyle={{ borderRadius: 16 }}>
                        <Text style={styles.detail}>
                            Enjoy the journey
                        </Text>
                        <Text style={styles.extraDetail}>
                            การวางแผนท่องเที่ยวจะไม่ใช่เรื่องยากอีกต่อไป
                        </Text>
                    </ImageBackground>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    img: {
        width: 325,
        height: 115
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    detail: {
        paddingTop: 25,
        textAlign: 'center',
        fontFamily: 'Thonburi',
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white'
    },
    extraDetail: {

        textAlign: 'center',
        fontFamily: 'Thonburi',
        fontSize: 13,
        fontWeight: 'bold',
        color: 'white'
    }
})

module.export = PredictTravelCard